#!/usr/bin/env ruby

require 'thread'
require './Timer'

class BufferError < RuntimeError ; end


class DBQueue
    attr_accessor :highwater, :flushInterval

    def initialize(highwater = nil, flushInterval = 60)
        @highwater     = highwater
        @flushInterval = flushInterval

        @queue         = Queue.new # use these because they are threadsafe
        @connection    = nil         # this is the database connection
        @flushing      = Mutex.new
        @locked        = true      # locked when not connected or in the process of disconnecting

        return self
    end

    def start
        return unless flushInterval
        @timer = Thread.new { Timer.every(@flushInterval) { flush } }
    end

    def stop
        if @timer
            Thread.kill(@timer)
            flush
        end
    end

    def empty?
        # return a boolean indicating whether the queue is empty or not
        return @queue.empty?
    end

    def full?
        # return a boolean indicating whether the queue is full or not
        return (@highwater && @queue.length >= @highwater) || false
    end

    def length
        return @queue.length
    end

    def add(data)
        # append some data to the queue

        if @locked
             raise BufferError, "buffer is locked"
        end

        @queue << data

        if full?
            flush
        end

        return self
    end

    alias :<< :add

    def clear
        # empty the queue without flushing
        @queue.clear
        return self
    end

    def connect(dsn)
        # attempt to connect to the database with the given DSN"

        if @connection
            raise BufferError, "Already connected"
        end

        begin
            @connection = get_connection(dsn)
            @locked = false
        rescue Exception => e
            raise BufferError, "Could not connect: #{e}"
        end

        start

        return self
    end

    def disconnect
        # attempt to disconnect from the database
        # This will *lock* the queue and allow no more incoming data during
        # the operation.  Forces a queue flush and commit as well

        unless @connection
            raise BufferError, "Not connected"
        end

        # # First, lock us to incoming data
        @locked = true

        stop
        flush

        begin
            drop_connection
        rescue Exception => e
            raise BufferError, "Unable to disconnect: #{e}"
        end

        @connection = nil

        return self
    end

    def flush
        # cycle through the queue, FIFO and flush it to the database"

        # @flushing is a cheap mutex to prevent us from invoking this faster than we can flush

        if empty? or @flushing.locked?
            return self
        end

        @flushing.synchronize do
            # take a snapshot of the queue length and flush to that point.  This prevents
            # races, where entries are being added as fast as they are being flushed
            #
            # we specifically do NOT lock the queue itself with a mutex, so that we can continue
            # to write to it while flushing.

            num_entries = @queue.length

            data = []
            num_entries.times { data << @queue.shift }

            save(data)
        end

        return self
    end


    protected

    ### virtual methods
    def get_connection(dsn)
        # connect to our remote database
        raise "You must override this method"
    end

    def drop_connection
        raise "You must override this method"
    end

    def save(data)
        raise "You must override this method"
    end
end

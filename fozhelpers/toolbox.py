# Copyright (c) 2010, Gary D. Foster <gary.foster@gmail.com>
# Licensed under the MIT license

"""
Summary: General purpose utility functions (not project specific)
"""

import datetime
import grp
import httplib
import htmllib
import logging
import logging.handlers
import os
import pwd
import re
import sys
import time
import traceback
import urllib
import urllib2
import smtplib
from email.mime.text import MIMEText

from urlparse import urlparse

def setupLogging(logfile="./logfile.log", loglevel=logging.INFO, stdout=False, syslog=False, when='midnight', interval=1, backupCount=6):
    """
    Set up the logging service.  Returns a log object.  Set stdout and syslog to True to add add'l streams.
    Defaults to a timed rotating log file rotating daily at midnight with 6 backup copies.

    You can write to the log object with log.<level>("message").  Valid levels are (debug|info|warning|error|critical)
    """

    log = logging.getLogger(logfile)

    log.setLevel(loglevel)
    formatter = logging.Formatter('[%(asctime)-12s] %(levelname)-8s <%(module)s#%(funcName)s:%(lineno)s> %(message)s')

    # logging.handlers.RotatingFileHandler(filename, mode, maxBytes, backupCount, encoding) can be used for rotating based on file size
    filelog = logging.handlers.TimedRotatingFileHandler(logfile, when=when, interval=interval, backupCount=backupCount)
    filelog.setFormatter(formatter)

    log.addHandler(filelog)

    if syslog:
        sl = logging.handlers.SysLogHandler('/dev/log')
        sl.setFormatter(formatter)
        log.addHandler(sl)

    if stdout:
        so = logging.StreamHandler(sys.stdout)
        so.setFormatter(formatter)
        log.addHandler(so)

    return log

def isBlank(arg):
    argType = type(arg).__name__


    if argType == 'str':
        return True if len(arg.strip()) == 0 else False
    elif argType == 'list' or argType == 'dict':
        return True if len(arg) == 0 else False
    elif argType == 'NoneType':
        return True
    else:
        raise ValueError, "illegal type: %s" % (argType)

def fetchRemoteFile(url, localFile=None, permissions=None, owner=None, group=None, retries=0):
    "Fetch a remote file and either write it to a local file or return it"
    # if token:
    #     cookie = "iPlanetDirectoryPro=%s" % (token)
    # else:
    #     cookie = None
    while retries > -1:
        try:
            if localFile:
                contents = urllib.URLopener()
                # if cookie:
                #     contents.addheader('Cookie', cookie)
                contents.retrieve(url, localFile)
                contents.close()
                if permissions:
                    os.chmod(localFile, permissions)
                if owner or group:
                    chown(localFile, owner=owner, group=group)
                return
            else:
                request = urllib2.Request(url)
                if cookie:
                    request.add_header('Cookie', cookie)
                response = urllib2.urlopen(request)
                fileContents = response.read()
                actualUrl = response.geturl()
                if actualUrl != url:
                    raise IOError, ('http error',302, "resource was redirected")
                response.close()
                return fileContents
        except Exception, e:
            # Ignore exceptions, we will throw an exception if retries
            # are exceeded only
            if retries != 0:
                retries -= 1
                pass
            else:
                raise

def formatExcPlus(exc_info):
    """
    Return descriptive exception info, including all locals, frame info and
    traceback.  Pass this method an exc_info object and it will pretty print
    an entire stack trace with values of every local variable in scope at each
    stack level.
    """
    (type_, value_, tb) = exc_info

    tb_info = "Exception type: %s\nValue: %s\n\nTraceback Info:\n" % (type_, value_)

    tb_info += "".join(traceback.format_tb(tb))
    tb_info += "\n"

    while True:
        if not tb.tb_next:
            break
        tb = tb.tb_next
    stack = []
    f = tb.tb_frame
    while f:
        stack.append(f)
        f = f.f_back
    stack.reverse()

    tb_info += "Locals by frame, innermost last\n"
    for frame in stack:
        tb_info += "\n"
        tb_info += "Frame %s in %s at line %s\n" % (frame.f_code.co_name,
            frame.f_code.co_filename, frame.f_lineno)
        for key, value in frame.f_locals.items():
            tb_info += "\t%20s = " % (key)
            try:
                tb_info += "%s\n" % (str(value))
            except:
                tb_info += "<ERROR WHILE PRINTING VALUE>\n"
    return tb_info

def grep(string, list, opt=''):
    "search a list for a substring.  Pass -v as an option to return everything BUT the match"

    expr = re.compile(string)
    if opt=='-v':
        nexpr = lambda x: expr.search(x) == None
        return filter(nexpr,list)
    return filter(expr.search,list)

def timestamp():
    return datetime.datetime.today().strftime("%D %T")

def generateDate(offsetDays=0):
    "alias function for generateHttpDate"
    return generateHttpDate(offsetDays)

def generateHttpDate(offsetDays=0):
    "Generate an httpdate formatted date string, offsetDays from now (in UTC/GMT)."
    return (datetime.datetime.utcnow() + datetime.timedelta(days=offsetDays)).strftime('%a, %d %b %Y %T GMT')

def generateIso8601Date(offsetDays=0):
    "Generate an ISO-8601 formatted date string in UTC, offsetDays from now yyyy-mm-dd'T'HH:MM:SS.fff'Z'."

    return (datetime.datetime.utcnow() + datetime.timedelta(days=offsetDays)).isoformat()+"Z"

def generateInternetTime():
    "Generate a timestamp in internet time format (beats)"
    t =  datetime.datetime.utcnow() + datetime.timedelta(seconds=3600) # Biel, Switzerland
    midnight = datetime.datetime(year=t.year, month=t.month, day=t.day)
    secs = (t - midnight).seconds
    beats = int(secs/86.4)
    return beats

def writeFile(filename, contents='', owner=None, group=None, permissions=None, append=False):
    # os.chmod() second param is OCTAL
    # python automatically converts 0644 from octal into decimal

    close = False

    if type(filename).__name__ == "str":
        # we got passed a filename to create
        if append:
            mode = "a"
        else:
            mode = "w"

        fd = open(filename, mode)
        name = fd.name
        # if we create the file, we close it, otherwise we leave it open
        close = True
    elif type(filename).__name__ == "file":
        # we got passed an open file descriptor
        fd = filename
        name = fd.name
    elif type(filename).__name__ == 'instance' and type(filename.file).__name__ == 'file':
        # we got passed a temporary file instance created by the tempfile module
        # which isn't 'quite' the same as a normal FD although it acts like one
        fd = filename
        name = filename.name
    else:
        raise TypeError, "filename must be a file object or a filename (string)"

    fd.write(contents)

    if close:
        fd.close()

    if permissions:
        os.chmod(name, permissions)

    if owner or group:
        chown(name, owner, group)

def chown(filename, owner=None, group=None):
    # -1 to chown means don't change it

    uid = pwd.getpwnam(owner).pw_uid if owner else -1
    gid = grp.getgrnam(group).gr_gid if group else -1

    os.chown(filename, uid, gid)

def getEc2PublicHostName():
    return fetchRemoteFile("http://169.254.169.254/2009-04-04/meta-data/public-hostname")

#####
##### SSO Methods
#####

def getSSOToken(uri, username, password):
    """
    Attempt to retrieve a valid token from an SSO server.
    Pass in the base URI to the sso realm (e.g. http://ssoserver.foo.com/opensso)
    and the username and password.  Returns None if invalid login
    """

    uri += "/identity/authenticate?username=%s&password=%s" % (username, password)

    try:
        req = urllib2.Request(uri)
        response = urllib2.urlopen(req)
        body = response.read()
        token = body.split('=', 1)[1].rstrip("\n")
    except urllib2.HTTPError, e:
        if e.code == 401:
            # Invalid login
            return None
        else:
            raise
    else:
        return token

def verifySSOToken(uri, token):
    "Attempt to verify a token against a base SSO uri"

    uri += "/identity/isTokenValid"

    data = urllib.urlencode({'tokenid': token})

    try:
        req = urllib2.Request(uri, data)
        response = urllib2.urlopen(req)
        page = response.read()
    except urllib2.HTTPError, e:
        if e.code == 401:
            valid=False
        else:
            raise
    else:
        body = page.split("=", 1)[1].rstrip("\n")
        if body.lower() == 'true':
            valid = True
        else:
            valid = False

    return valid

def invalidateSSOToken(uri, token):
    "Logout and invalidate a previously generated token"

    uri += "/identity/logout"

    data = urllib.urlencode({'subjectid': token})

    try:
        req = urllib2.Request(uri, data)
        response = urllib2.urlopen(req)
    except urllib2.HTTPError, e:
        return e.code
    else:
        return response.code

def authorizeSSOResource(uri, token, action):
    "Determine if given action is authorized to token at uri"

    uri += "/identity/authorize"

    if action.upper() in ['GET', 'PUT', 'POST', 'DELETE', 'OPTIONS', 'HEAD']:
        try:
            data = urllib.urlencode({'uri': uri, 'action': action.upper(), 'subjectid': token})
            req = urllib2.Request(uri, data)
            response = urllib2.urlopen(req)
            page = response.read()
        except urllib2.HTTPError, e:
            valid = False
        else:
            body = page.split("=", 1)[1].rstrip("\n")
            if body.lower() == 'true':
                valid = True
            else:
                valid = False
    else:
        raise ValueError, "Action must be one of GET/POST/PUT/DELETE/OPTIONS/HEAD"

    return valid

def urlEscape(uri):
    "Properly escapes a URI string and returns the escaped version"

    return urllib.quote(urllib.unquote(uri), safe=":/")

def installPackage(packages):
    "accepts either a list or a single package and installs it"

    if type(packages).__name__ == 'str':
        packages = [packages]

    for pkg in packages:
        rc = runCmd("! /usr/bin/yum install -y --nogpgcheck %s | egrep '^No package .+ available\.' 2>&1 >/dev/null " % (pkg))
        if rc:
            # if we get an error, exit prematurely
            return rc

def writeTaskProgress(filename, caller, taskName, percentComplete, startTime=generateIso8601Date()):
    fd = open(filename, "a")
    fd.write("%s\t%s\t%s\t%s\t%s\n" % (generateIso8601Date(), caller, percentComplete, startTime, taskName))
    fd.close()

def exitIf(rc):
    if rc != 0:
        sys.exit(rc)

def runCmd(command, ignoreRC=True, asUser=None):
    if asUser:
        # Need to escape any double quotes
        if command.find('"') != -1:
            command = command.replace('"', '\\"')
        command = "/sbin/runuser -s /bin/bash - %s -c \"%s\"" % (asUser, command)

    rc = os.system(command)
    if not ignoreRC and rc:
        raise OSError, "%s returned a non-zero RC: %d" % (command, rc)
    else:
        return rc

def addUser(username, uid=None, group=None, shell=None, homeDir=None, createHome=False):
    cmd = "/usr/sbin/useradd "
    cmd += "-u %d" % (uid) if uid else " "
    cmd += "-g %s" % (group) if group else " "
    cmd += "-s %s " % (shell) if shell else " "
    cmd += "-d %s " % (homeDir) if homeDir else " "
    cmd += "-m " if createHome else "-M "
    cmd += username

    rc = runCmd(cmd)
    return rc

def addGroup(groupname, gid=None):
    cmd = "/usr/sbin/groupadd "
    cmd += "-g %d" % (gid) if gid else " "
    cmd += groupname

    rc = runCmd(cmd)
    return rc

def setPassword(username, password):
    cmd = "echo '%s' | /usr/bin/passwd --stdin %s" % (password, username)
    rc = runCmd(cmd)
    return rc

def removeTimestamp(filename):
    # Remove timestamp added by webresource , e.g 2011-01-25T13-31-20-latest.tar.gz
    tsPattern=r'^(19|20)\d\d[- / .](0[1-9]|1[012])[- / .](0[1-9]|[12][0-9]|3[01])T(0[0-9]|1[0-9]|2[0-4])[- / .](0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]|60)[- / .](0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]|60)-'

    return re.sub(tsPattern, '', filename)

def sendMail(mail_from, mail_to, subject, body, smtp_host, smtp_user=None, smtp_password=None, useTLS=False):
    "Connects to a remote SMTP server and sends email."

    msg = MIMEText(body)

    msg['From']    = mail_from
    msg['To']      = mail_to
    msg['Subject'] = subject

    smtp = smtplib.SMTP(smtp_host)

    try:
        if useTLS:
            smtp.start_tls()

        if smtp_user and smtp_password:
            smtp.login(smtp_user, smtp_password)

        smtp.sendmail(mail_from, [mail_to], msg.as_string())
    finally:
        smtp.quit()

def html_unescape(s):
    p = htmllib.HTMLParser(None)
    p.save_bgn()
    p.feed(s)
    return p.save_end()

def dottedQuadToNum(ip):
    "convert decimal dotted quad string to long integer"
    return struct.unpack('!L',socket.inet_aton(ip))[0]

def numToDottedQuad(n):
    "convert long int to dotted quad string"
    return socket.inet_ntoa(struct.pack('=L',n))

def calcDottedNetmask(mask):
    bits = 0
    for i in xrange(32-mask,32):
        bits |= (1 << i)
    return "%d.%d.%d.%d" % ((bits & 0xff000000) >> 24, (bits & 0xff0000) >> 16, (bits & 0xff00) >> 8 , (bits & 0xff))

def makeMask(n):
    "return a mask of n bits as a long integer"
    return (2L<<n-1)-1

def ipToNetAndHost(ip, maskbits):
    "returns tuple (network, host) dotted-quad addresses given IP and mask size"
    # (by Greg Jorgensen)

    n = dottedQuadToNum(ip)
    m = makeMask(maskbits)

    host = n & m
    net = n - host

    return numToDottedQuad(net), numToDottedQuad(host)

def addressInNetwork(ip,net):
    """"
    This function allows you to check if on IP belogs to a Network
    addressInNetwork("192.168.100.16", "192.168.0.0/16") == True
    """
    ipaddr = struct.unpack('!L',socket.inet_aton(ip))[0]
    netaddr,bits = net.split('/')
    netmask = struct.unpack('!L',socket.inet_aton(calcDottedNetmask(int(bits))))[0]
    network = struct.unpack('!L',socket.inet_aton(netaddr))[0] & netmask
    return (ipaddr & netmask) == (network & netmask)

class Struct(object):
    "Python doesn't have a struct class, so we will fake one"

    def __init__(self, **entries):
        self.__dict__.update(entries)

    def __repr__(self):
        args = ['%s=%s' % (k, repr(v)) for (k,v) in vars(self).items()]
        return '%s(%s)' % (self.__class__.__name__, ', '.join(args))

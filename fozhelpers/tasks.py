class Task(object):
    def __init__(self, name='', status='pending', percent=0, startTime='', endTime=''):
        self.name      = name
        self.status    = status
        self.percent   = percent
        self.startTime = startTime
        self.endTime   = endTime

    def __repr__(self):
        args = ['%s=%s' % (k, repr(v)) for (k,v) in vars(self).items()]
        return '%s(%s)' % (self.__class__.__name__, ', '.join(args))

    def __str__(self):
        return "%s\t%s\t%s\t%s\t%s" % (self.name, self.status, self.percent, self.startTime, self.endTime)

    def start(self):
        "Mark the current task as active"
        self.status = 'active'
        self.startTime = generateIso8601Date()

    def finish(self):
        "Mark the task as complete"
        self.status = 'complete'
        self.percent = 100
        self.endTime = generateIso8601Date()

    def fail(self):
        "Mark the task as failed"
        self.status = 'failed'
        self.endTime = generateIso8601Date()

    def clear(self):
        "Clear the and set it to pending"
        self.status    = 'pending'
        self.percent   = 0
        self.startTime = ''
        self.endTime   = ''


class Tasklist(object):
    def __init__(self, filename=None):
        self._tasks = []
        self._currentTask = -1
        self.filename = filename
        self.__status = 0

    def __iter__(self):
        for index in range(0, len(self._tasks), 1):
            yield self._tasks[index]

    def __repr__(self):
        args = ['%s=%s' % (k, repr(v)) for (k,v) in vars(self).items()]
        return '%s(%s)' % (self.__class__.__name__, ', '.join(args))

    def __str__(self):
        res = ""
        for task in self:
            res += str(task)+"\n"
        res=res[:-1]
        return res

    def _getStatus(self):
        return self.__status

    def add(self, task):
        "Add a task object to the queue"

        argType = type(task).__name__

        if argType == 'str':
            self._tasks.append(Task(task))
        elif argType == 'Task':
            self._tasks.append(task)
        else:
            raise ValueError, "Can only add tasks to tasklist"

        return self

    def delete(self, taskNumber=None):
        "Delete a task from the queue (no task number passed in means delete the current task)"
        if taskNumber is None:
            taskNumber = self._currentTask

        if isBlank(self._tasks):
            raise ValueError, "Attempt to delete empty task"

        if taskNumber > len(self._tasks) - 1 or taskNumber < 0:
            raise ValueError, "attempt to delete invalid task"

        if taskNumber == self._currentTask:
            self.next()
            self._currentTask -= 1

        del(self._tasks[taskNumber])
        self.save()

        self.__status = int(float(self._currentTask) / len(self._tasks) * 100)

        return self

    def next(self):
        "Mark the current task as complete and advance the task counter"
        if self._currentTask >= 0:
            self._tasks[self._currentTask].finish()

        self._currentTask += 1

        if self._currentTask < len(self._tasks):
            self.__status = int(float(self._currentTask) / len(self._tasks) * 100)
            self._tasks[self._currentTask].start()
        else:
            self._currentTask = -1

        self.save()

    def finish(self):
        "Mark all remaining tasks complete and halt the queue"
        if self._currentTask == -1:
            return

        for index in range(self._currentTask, len(self._tasks), 1):
            self.next()

        self.__status = 100

        self.save()

    def abort(self):
        "Fail the current task and halt the queue by setting the task pointer to None"
        if self._currentTask < 0:
            self._currentTask = 0

        self._tasks[self._currentTask].fail()
        self._currentTask = -1
        self.save()

        return self

    def start(self):
        "Reset all the tasks in the queue to pending and move the task pointer to the beginning"
        # clear out the task list status flags and times on a start
        for index in range(0, len(self._tasks), 1):
            self._tasks[index].clear()

        self._currentTask = 0
        self._status = 0
        self._tasks[self._currentTask].start()
        self.save()

        return self

    def currentTask(self):
        "Return the current task object being worked on"
        if self._currentTask < 0:
            return None
        else:
            return self._tasks[self._currentTask]

    def save(self, filename=None):
        "Marshal ourselves to a file"
        if filename is None:
            filename = self.filename

        if filename is not None:
            contents = str(self._currentTask) + "\n"
            contents += str(self)
            fd = open(filename, 'w').write(contents)

            return self

    def load(self, filename=None):
        "Demarshal ourselves from a previously saved file"
        if filename is None:
            filename = self.filename

        if filename is not None:
            fd = open(filename, 'r')
            vals = fd.readlines()
            fd.close()
            # now strip all the trailing newlines
            vals = map(lambda x: x.rstrip("\n"), vals)
            self.clear()
            # First line is the task pointer
            self._currentTask = int(vals.pop(0))
            # rest of vals contains the individual task
            for task in vals:
                (name, status, percent, startTime, endTime) = task.split("\t")
                self.add(Task(name=name, status=status, percent=percent, startTime=startTime, endTime=endTime))

    def clear(self):
        "Empty out the task list"
        self._tasks = []
        self._currentTask = -1

        return self

    status = property(lambda self: self._getStatus())

def get_connection(dbname, server=None):
    "open a connection to a mongo DB running locally and return the handle"
    connection = pymongo.Connection(server)
    db = getattr(connection, dbname)

    # why was I returning collection here?  Am I deliberately a dumbass?
    # db = getattr(db, dbname)

    return db

def get_collection(dbname, collection, server=None):
    "return a handle directly to the named dbname.collection"
    db = get_mongo_connection(dbname, server=server)
    collection = getattr(db, collection)
    return collection

def store_this(dbname, collection, data, server=None):
    "Store json <data> in mongo <collection> on <dbname>"
    db = get_mongo_collection(dbname, collection, server=server)

    # This returns an ID pointer, but it is in bson.objectiid.ObjectId format
    # so we'll cast it to a string

    obj_id = str(db.save(data))
    return obj_id

def fetch_by_id(dbname, collection, id, server=None):
    "Retrieve the object <id> from the mongo <collection> in <dbname>"

    db = get_mongo_collection(dbname, collection, server=server)
    #oid = bson.objectid.ObjectId(id)
    result = db.find_one(id)
    return result

def fetch_by_key(dbname, collection, server=None, conditions={}, fields=None, limit=0):
    "fetch stuff from the mongo collection selected by limiters in <conditions>"
    # condtions is a dict
    # fields is a list of fields to limit in the query (will only return those fields)
    # 0 limit is no limit, return everything

    db = get_mongo_collection(dbname, collection, server=server)

    result = []
    result.extend(db.find(conditions, fields=fields).limit(limit))

    return result

def delete_by_id(dbname, collection, obj_id, server=None):
    "Delete object <id> from <collection>"

    db = get_mongo_collection(dbname, collection, server=server)
    #oid = bson.objectid.ObjectId(obj_id)
    db.remove(obj_id)
